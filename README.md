# Greenpay


### Description

The API expose:

* The user list read for admin users
* The transaction creation for all authenticated users

50 Users are created on install process from 0 to 49. with those access:
* login `user_{n}` ex: `user_1`
* password `pwd{n}` ex: `pwd1`

All admins are user whose `{n}` is a multiple of 10

### Installation

Install the environnement, docker containers, dependencies, database and load fixtures with dummy data.

```sh
$ make install
```

to update database and sources...

```sh
$ make update
```
and for reinstall
```sh
$ make reinstall
```

### Routes

Availables routes.

| Route | Verb | Purpose |
| ------ | ------ | ----- |
| /auth | POST |Authenticate to api  |
| /logout | GET| To log out |
| /user | GET |The users list |
| /transaction | POST | To create a transaction item |


