
update:
	docker-compose up -d
	docker-compose exec php-fpm composer --working-dir=./api install
	docker-compose exec php-fpm php ./api/bin/console doctrine:migration:migrate -n

install: update
	docker-compose exec php-fpm php ./api/bin/console doctrine:fixtures:load -n

kill:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml kill

reinstall: kill install

phpcs:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml run phpcs

