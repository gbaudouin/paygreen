<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * App exception listener.
 */
class ExceptionListener
{
    /**
     * Kernel exception event handler.
     * @param  ExceptionEvent $event The exception event.
     */
    public function onKernelException($event)
    {
        $exception = $event->getThrowable();

        if ($exception) {
            $response = new JsonResponse();
            $response->setData([
                'code' => $exception->getCode(),
                'exception' => get_class($exception),
                'violations' => [
                    'message' => $exception->getMessage(),
                ],
            ]);
            $event->setResponse($response);
        }
    }
}
