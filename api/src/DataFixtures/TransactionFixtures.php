<?php

namespace App\DataFixtures;

use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TransactionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();
        $faker = \Faker\Factory::create();

        foreach ($users as $user) {
            for ($i=0; $i < random_int(1, 10); $i++) {
                $transaction = new Transaction();

                $transaction->setName($faker->word());
                $transaction->setOwner($user);
                $transaction->setStatus((random_int(0, 1) ? true : false));
                $transaction->setDescription($faker->paragraph());
                $transaction->setAmount($faker->randomFloat(2, 1, 10000));

                $manager->persist($transaction);
            }
        }


        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}
