<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UserController extends AbstractController
{

    /**
     *
     * @var SerializerInterface
     */
    protected $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/user", name="user_list", methods={"GET"})
     *
     * @IsGranted("ROLE_ADMIN", message="You are not allowed to perform this action")
     */
    public function list(Request $request): Response
    {
        $page = intval($request->query->get('page', 0));
        $item_per_page = intval($request->query->get('item_per_page', 10));

        if ($item_per_page > 50) {
            return $this->json([
                'success' => false,
                'message' => 'You can only retrieve a maximum 50 users',
            ]);
        }

        $repo = $this->getDoctrine()->getRepository(User::class);
        // @todo implements filters.
        $collection = $repo->findBy([], null, $item_per_page, ($page * $item_per_page));

        $users = $this->serializer->normalize($collection, null, ['groups' => ['user:read']]);

        return new JsonResponse($users);
    }
}
