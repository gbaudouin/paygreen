<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpFoundation\Request;

class ApiBaseController extends AbstractController
{
    /**
     * Retrieve violation list collection.
     *
     * @param ConstraintViolationListInterface $errors
     *   The errors collection.
     *
     * @return array
     *   The violation list to array.
     */
    protected function getViolationsList(ConstraintViolationListInterface $errors)
    {
        return array_map(function ($error) {
            return [
                'property' => $error->getPropertyPath(),
                'message' => $error->getMessage(),
            ];
        }, $errors->getIterator()->getArrayCopy());
    }

    /**
     * Retrieve json data or JsonResponse if invalid.
     *
     * @param Request $request
     * @return JsonResponse|\stdClass
     */
    protected function validateJson(Request $request)
    {
        if ($request->getContentType() !== 'json') {
            return $this->json([
                'success' => false,
                'violations' => ['message' => 'Invalid request format'],
            ]);
        }

        $content = json_decode($request->getContent());
        
        if (empty($content)) {
            return $this->json([
                'success' => false,
                'violations' => ['message' => 'Invalid json'],
            ]);
        }

        return $content;
    }
}
