<?php

namespace App\Controller;

use App\Entity\Transaction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class TransactionController extends ApiBaseController
{
    //* @Route("/transaction.{_format}", name="create_transaction", methods={"POST"}, format="html", requirements={"_format": "html/xml"})
    /**
     * @Route(
     *     "/transaction.{_format}",
     *     format="json",
     *     requirements={
     *         "_format": "json"
     *     }
     * )
     * @IsGranted("ROLE_USER", message="You are not allowed to perform this action")
     */
    public function createTransation(Request $request, ValidatorInterface $validator): Response
    {
        $json = $this->validateJson($request);

        if ($json instanceof JsonResponse) {
            return $json;
        }

        $transaction = new Transaction();
        $transaction->setOwner($this->getUser());
        $transaction->setStatus(true);

        if (isset($json->name)) {
            $transaction->setName($json->name);
        }
        if (isset($json->description)) {
            $transaction->setDescription($json->description);
        }
        if (isset($json->amount)) {
            $transaction->setAmount($json->amount);
        }

        $errors = $validator->validate($transaction);

        if ($errors->count() > 0) {
            return $this->json([
                'success' => false,
                'violations' => $this->getViolationsList($errors),
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($transaction);
        $em->flush();

        return $this->json([
            'success' => true,
            'id' => $transaction->getId(),
            'message' => 'Transaction ' . $transaction->getName() . ' successfully created',
        ]);
    }
}
