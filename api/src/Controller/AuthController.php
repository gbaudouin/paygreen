<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /**
     * @Route("/auth", name="auth", methods={"POST"})
     */
    public function login(): Response
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        return $this->json([
            'success' => true,
            'roles' => $user->getRoles(),
            'message' => 'Welcome on board ' . $user->getFirstName() . ' ' . $user->getLastName() . '!',
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('Log out');
    }
}
